# OpenML dataset: Metamaterial-Antennas

https://www.openml.org/d/43646

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Microstrip Antennas are low-profile antennas applied in high-performance aircraft, spacecraft, satellite and missile applications, where size, weight, performance, ease of installation and aerodynamic profile are constraints. Civilian applications includes mobile communication and wireless connection.
Metamaterials are specific dispositions of unit cells with peculiar electrical properties. These materials present properties that doesnt exist on nature. Because his dispositions, electrical permissivity and magnetic permeability are negatives. This produces negative refraction, which makes the eletromagnetic radiation behave diferent from expected.
Inspiration
In order to optimize antenna projetcs, why not apply ML?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43646) of an [OpenML dataset](https://www.openml.org/d/43646). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43646/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43646/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43646/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

